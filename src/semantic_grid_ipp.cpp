#include "ros/ros.h"
#include "semantic_grid/camera_interface.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include "semantic_grid/shared_semanticmap.hpp"
#include "semantic_grid/simple_planner.h"
#include "tf_utils/tf_utils.h"
#include "informative_path_planner_utils/ipp_wrapper.h"

nav_msgs::Odometry lookahead_odo;

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  lookahead_odo = *msg;
}

visualization_msgs::Marker InitializeRayMarker(std::string frame, int id=0){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "rays";
    marker.frame_locked = true;
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 0.1; // Don't forget to set the alpha!
    if(id ==0){
        marker.color.r = 1.0;
        marker.color.g = 1.0;
        marker.color.b = 1.0;
    }else{
        marker.ns = "global_rays";
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
    }
    return marker;
}


visualization_msgs::Marker MakePath(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, std::string frame_id, Eigen::Vector3d pos, ca::SemanticClass cc){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_id;
    m.header.stamp = ros::Time();
    m.ns = "to_car";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::LINE_STRIP;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    m.color.b = 0.0;
    geometry_msgs::Point p;
    p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
     m.points.push_back(p);
    for(size_t i=0; i < grid->dim_i(); i++){
        for(size_t j=0; j < grid->dim_j(); j++){
            ca::FixedGrid2f::Vec2 v = grid->grid_to_world(i,j);
            ca::SemanticGridCell c = grid_data[grid->grid_to_mem(i,j)];
            int max_id = 0;
            float p_max = 0.0;
            for(size_t i=0; i< 4; i++)
            {
                if(c.p_classes_[i] > p_max){
                    max_id = i;
                    p_max = c.p_classes_[i];
                }
            }
            if(max_id == (int)cc){
                //ROS_INFO_STREAM("Found a car!");
                p.x = v.x(); p.y = v.y(); p.z = c.height_mean_-5;
                m.points.push_back(p);
                break;
            }
        }
    }

    return m;
}


visualization_msgs::Marker InitializeImageCheckMarker(std::string frame, int id=0){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame;
    marker.header.stamp = ros::Time();
    marker.ns = "image";
    marker.frame_locked = true;
    marker.id = id;
    marker.type = visualization_msgs::Marker::CUBE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.1;
    marker.scale.y = 0.1;
    marker.scale.z = 0.1;
    marker.color.a = 0.01; // Don't forget to set the alpha!
    if(id ==0){
        marker.color.r = 1.0;
        marker.color.g = 1.0;
        marker.color.b = 1.0;
    }else{
        marker.ns = "global_rays";
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;
    }
    return marker;
}


void AddRay2Marker(visualization_msgs::Marker& m, Eigen::Vector3d& ray_begin, Eigen::Vector3d& ray_end)
{
    geometry_msgs::Point p; p.x=ray_begin.x(); p.y=ray_begin.y(); p.z=ray_begin.z();
    geometry_msgs::Point p1; p1.x= p.x + 100*ray_end.x();
    p1.y= p.y + 100*ray_end.y(); p1.z= p.z + 100*ray_end.z();
    m.points.push_back(p); m.points.push_back(p1);
}

void AddPixel2Marker(visualization_msgs::Marker& m, ca::SemanticClass cc, Eigen::Vector3d& ray_begin, Eigen::Vector3d& ray_end)
{
    geometry_msgs::Point p; p.x=ray_begin.x(); p.y=ray_begin.y(); p.z=ray_begin.z();
    geometry_msgs::Point p1; p1.x= p.x + 5*ray_end.x();
    p1.y= p.y + 5*ray_end.y(); p1.z= p.z + 5*ray_end.z();
    m.points.push_back(p1);
    std_msgs::ColorRGBA c;
    switch(cc) {
        case ca::SemanticClass::car : c.r = 1.0; c.g=0; c.b=0; c.a = 0.8; break;
        case ca::SemanticClass::building : c.r = 0.0; c.g=0; c.b=1.0; c.a = 0.8; break;
        case ca::SemanticClass::road : c.r = 0.3; c.g=0.3; c.b=0.3; c.a = 0.8; break;
        case ca::SemanticClass::vegetation : c.r = 158.0/255.0; c.g= 83.0/255.0; c.b= 58.0/255.0; c.a = 0.8; break;
        default: c.r = 1.0; c.g= 1.0; c.b= 1.0; c.a = 0.8; break;
    }
    m.colors.push_back(c);
}


std_msgs::ColorRGBA SemanticColor(ca::SemanticGridCell sm, ca::SemanticClass& class_max){
    std_msgs::ColorRGBA c;
    size_t max_id = 0;
    double p_max = -1000.0;

    for(size_t i=0; i< sm.p_classes_.size(); i++)
    {
        if(sm.p_classes_[i] > p_max){
            max_id = i;
            p_max = sm.p_classes_[i];
        }
    }

    if(sm.p_classes_[2]> 0.85)
        max_id = 2;
    /*if(p_max < 1.0)
    {
        ROS_ERROR_STREAM("Class::"<<max_id<<" P::"<<p_max);
    }*/

    class_max = static_cast<ca::SemanticClass>(max_id);

    switch(class_max) {
        case ca::SemanticClass::car : c.r = 1.0; c.g=0; c.b=0; c.a = 0.8; break;
        case ca::SemanticClass::building : c.r = 0.0; c.g=0; c.b=1.0; c.a = 0.8; break;
        case ca::SemanticClass::road : c.r = 0.3; c.g=0.3; c.b=0.3; c.a = 0.8; break;
        case ca::SemanticClass::vegetation : c.r = 158.0/255.0; c.g= 83.0/255.0; c.b= 58.0/255.0; c.a = 0.8; break;

        default: c.r = 0.0; c.g= 0.0; c.b= 0.0; c.a = 1.0; break;
    }
    c.a = std::max(p_max,0.1);
    return c;
}

void ResetSemanticGridCell(ca::SemanticGridCell &c){
   c.p_classes_[(int)ca::SemanticClass::car] = 0.1;
   c.p_classes_[(int)ca::SemanticClass::building] = 0.1;
   c.p_classes_[(int)ca::SemanticClass::vegetation] = 0.7;
   c.p_classes_[(int)ca::SemanticClass::road] = 0.1;
   c.casted_ = false;
   c.min_observation_distance_ = std::pow(10,6);
   for(size_t i=0; i<4; i++){
       ca::ClassProperties &class_prop = c.class_properties_[i];
       class_prop.height_initialized_ = false;
       class_prop.upper_height_bound_ = -std::pow(10,6);
       class_prop.lower_height_bound_ = std::pow(10,6);
       class_prop.p_obs_for_ = 0.0;
       class_prop.num_obs_for_ = class_prop.num_obs_against_ = 0;
       class_prop.p_obs_against_ = 0.0;
    }   
}


visualization_msgs::Marker GetSemanticGridMarker(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, std::string frame_id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_id;
    m.header.stamp = ros::Time();
    m.ns = "semantic_grid";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::CUBE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 5;
    m.scale.y = 5;
    m.scale.z = 0.1;
    m.color.a = 0.5; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    geometry_msgs::Point p;
    for(size_t i=0; i < grid->dim_i(); i++){
        for(size_t j=0; j < grid->dim_j(); j++){
            ca::FixedGrid2f::Vec2 v = grid->grid_to_world(i,j);
            ca::SemanticGridCell c = grid_data[grid->grid_to_mem(i,j)];
            ca::SemanticClass class_max;
            std_msgs::ColorRGBA color = SemanticColor(c,class_max);
            if(false && c.casted_){
                color.r = 1.0;color.g=1.0;color.b=1.0;
                color.a = 1.0;
               grid_data[grid->grid_to_mem(i,j)].casted_ = false;
            }

            m.colors.push_back(color);
            p.x = v.x(); p.y = v.y(); p.z = c.height_mean_-c.class_properties_[(int)class_max].ReportHeight();
            m.points.push_back(p);
            ResetSemanticGridCell(grid_data[grid->grid_to_mem(i,j)]);
        }
    }

    return m;
}


visualization_msgs::Marker GetSemanticGridMarkerClass(ca::FixedGrid2f* grid, ca::SemanticGridCell* grid_data, std::string frame_id, std::string ns, ca::SemanticClass sc){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_id;
    m.header.stamp = ros::Time();
    m.ns = ns;
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::CUBE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 5;
    m.scale.y = 5;
    m.scale.z = 0.1;
    m.color.a = 0.5; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;
    geometry_msgs::Point p;
    std_msgs::ColorRGBA color;
    color.r = 1.0; color.g = 0.0; color.b = 0.0;
    for(size_t i=0; i < grid->dim_i(); i++){
        for(size_t j=0; j < grid->dim_j(); j++){
            ca::FixedGrid2f::Vec2 v = grid->grid_to_world(i,j);
            ca::SemanticGridCell c = grid_data[grid->grid_to_mem(i,j)];
            if(c.integrated_logodds_[(int)sc]>0){
                color.a = std::min(((double)c.integrated_logodds_[(int)sc])/500.0,1.0);
                color.r = 1.0; color.g = 0.0; color.b = 0.0;
            }
            else{
                color.a = std::min(-((double)c.integrated_logodds_[(int)sc])/500.0,1.0);
                color.r = 0.0; color.g = 0.0; color.b = 0.0;
            }
            m.colors.push_back(color);
            p.x = v.x(); p.y = v.y(); p.z = c.height_mean_-c.class_properties_[(int)sc].ReportHeight();
            m.points.push_back(p);
        }
    }

    return m;
}


bool Image2SemanticClass(uint8_t c, ca::SemanticClass& sc, double &p){
    if(c>60){
        sc = ca::SemanticClass::car;
        p = 0.7 + ((double)c-60)/255.0;
        p = std::min(p,1.0);

    }
    else{
        p= 0.5 + (170.0 - (double)c)/255.0;
        p = std::min(p,1.0);
        sc = ca::SemanticClass::vegetation;
    }
    return true;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "example_semantic_grid");
  ros::NodeHandle n("~");

  bool use_dem;
  if (!n.getParam("use_dem", use_dem)){
    ROS_ERROR_STREAM("Could not get use_dem parameter.");
    return -1;
  }

  LabelTensorInterface cmi;

  ros::Publisher rays_marker_pub = n.advertise<visualization_msgs::Marker>( "/semantic_map/camera_rays", 0 );
  ros::Publisher semantic_marker_pub = n.advertise<visualization_msgs::Marker>( "/semantic_map/semantic_map_vis", 0 );
  ros::Publisher exploration_marker_pub = n.advertise<visualization_msgs::MarkerArray>( "/exploration_planner/vis", 0 );
  ros::Publisher traj_marker_pub = n.advertise<visualization_msgs::MarkerArray>( "/exploration_planner/traj_vis", 0 );
  ros::Publisher exploration_path_pub = n.advertise<ca_nav_msgs::PathXYZVViewPoint>( "/global_planner/path", 0 );
  ros::Subscriber lookahead_sub = n.subscribe("/path_tracking_control_node/lookahead_pose", 1000, OdometryCallback);
  ros::Subscriber labeltensor_sub = n.subscribe("/labels",1,& LabelTensorInterface::LabelsTensorSub,&cmi);




  LocalFrameManager fm;
  ros::Rate r(5);
  if(use_dem){
      bool server = false;
      fm.Initialize(server,n);

      while(ros::ok() && !fm.IsInitialized()){
          ros::spinOnce();
          r.sleep();
      }
  }

  std::string grid_frame = "/semantic_grid";
  if (!n.getParam("grid_frame", grid_frame)){
    ROS_ERROR_STREAM("Could not get grid frame parameter.");
    return -1;
  }

  DEMData* dem_reader = new DEMData();
  if(use_dem){
      std::string dem_file;
      if (!n.getParam("dem_file", dem_file)){
        ROS_ERROR_STREAM("Could not get dem file parameter.");
        return -1;
      }

      if(!dem_reader->loadFile(0,dem_file))
      {
          ROS_ERROR_STREAM("Could not load DEM file");
          return -1;
      }
  }
  ca::SharedSemanticMap sgmap;
  sgmap.set_grid_frame(grid_frame);
  if(use_dem)
    sgmap.init(&fm,dem_reader);
  else
      sgmap.init();

  delete dem_reader;

  double sensing_range = 400;

  tf::TransformListener tf_listener(n);
  tf::StampedTransform stamped_camera2grid;

  SimplePlanner smp;
  if(!smp.SetParams(n))
  {
      ROS_ERROR_STREAM("Didn't get parameters for simple planner");
      exit(-1);
  }
  double prev_processed_im_stamp = 0.0;


  tf::StampedTransform stamped_camera2dji;
  std::string base_frame = "/base_link";
  std::vector<ca::ViewPoint> vp;
  ca::IPPWrapper ipp_wrapper(sgmap.GetFixedGrid(), sgmap.GetMem(),vp,grid_frame);

  while(!(cmi.Initialized() && ca::tf_utils::getCoordinateTransform(tf_listener,cmi.cam_model_.tfFrame(),base_frame,.2,
                                          stamped_camera2dji,cmi.cam_model_.stamp().toSec()))){
      ros::spinOnce();
      r.sleep();
  }

  tf::Transform transform = stamped_camera2dji;
  ipp_wrapper.Initialize(sgmap,cmi,transform,1.2,n);
  ipp_wrapper.generate_global_vp_list();



  while(ros::ok()){
      bool doing_cars = false;
    if (cmi.Initialized() &&
        ca::tf_utils::getCoordinateTransform(tf_listener,cmi.cam_model_.tfFrame(),grid_frame,.2, stamped_camera2grid,cmi.cam_model_.stamp().toSec())){
        tf::Transform transform = stamped_camera2grid;
        Eigen::Vector3d pos(0,0,0);

        visualization_msgs::Marker m;        
        if(cmi.Initialized()){
            if((cmi.cam_model_.stamp().toSec()- prev_processed_im_stamp)> 0.01){
                prev_processed_im_stamp = cmi.cam_model_.stamp().toSec();
                ca::tf_utils::transformPoint(transform, pos);
                m = InitializeRayMarker(cmi.cam_model_.tfFrame());
                visualization_msgs::Marker m_grid = InitializeRayMarker("/semantic_grid",1);
                visualization_msgs::Marker m_points = InitializeImageCheckMarker(cmi.cam_model_.tfFrame());

                Eigen::Vector3d zeros(0,0,0);
                for(size_t i=0; i<cmi.labels_data_layout_.dim[1].size;i++){
                    for(size_t j=0; j<cmi.labels_data_layout_.dim[2].size;j++){
                        ca::SemanticClass c = ca::SemanticClass::car;
                        double p=0.0;
                        bool valid = false;
                        uint8_t obs_class  = cmi.getClassProbability(i,j,1,valid); // 1 is car
                        if(Image2SemanticClass(obs_class,c,p))
                        {
                            doing_cars = true;
                            Eigen::Vector3d vv = cmi.getRayAt(i,j); std::swap(vv.x(),vv.y());
                            AddRay2Marker(m,zeros,vv);
                            AddPixel2Marker(m_points,c,zeros,vv);
                            ca::tf_utils::transformVector3d(transform,vv,true);
                            AddRay2Marker(m_grid,pos,vv);
                            sgmap.ProcessRay(vv,pos,sensing_range,c,p);
                        }
                    }
                }
                sgmap.ProcessToBeProcessed();
                sgmap.mem_to_process_.clear();
                rays_marker_pub.publish(m);
                rays_marker_pub.publish(m_grid);
                rays_marker_pub.publish(m_points);
            }
            /*tf::StampedTransform odo_to_grid;
            if( !lookahead_odo.header.frame_id.empty()   &&
                ca::tf_utils::getCoordinateTransform(tf_listener,lookahead_odo.header.frame_id,grid_frame,1,odo_to_grid))
            {
                transform = odo_to_grid;
                nav_msgs::Odometry odom = lookahead_odo;
                ca::tf_utils::transformOdom(transform,odom);
                Eigen::Vector3d lookahead_pos;
                lookahead_pos.x() =  odom.pose.pose.position.x;
                lookahead_pos.y() =  odom.pose.pose.position.y;
                lookahead_pos.z() =  odom.pose.pose.position.z;
                ca_nav_msgs::PathXYZVViewPoint path = smp.MakePath(sgmap.GetFixedGrid(), sgmap.GetMem(), grid_frame, lookahead_pos, ca::SemanticClass::car);
                if(path.waypoints.size()>0)
                    exploration_path_pub.publish(path);
                m = smp.MakePathMarker(path);
                semantic_marker_pub.publish(m);
                m = smp.CarMarker(smp.GetCarLocation(),grid_frame);
                semantic_marker_pub.publish(m);
            }*/
        }

        tf::StampedTransform odo_to_grid;
        if( !lookahead_odo.header.frame_id.empty()   &&
            ca::tf_utils::getCoordinateTransform(tf_listener,lookahead_odo.header.frame_id,grid_frame,1,odo_to_grid))
        {
            transform = odo_to_grid;
            nav_msgs::Odometry odom = lookahead_odo;
            ca::tf_utils::transformOdom(transform,odom);

            Eigen::Vector3d start_point;
            start_point.x()= odom.pose.pose.position.x;
            start_point.y()= odom.pose.pose.position.y;
            start_point.z()= odom.pose.pose.position.z;

            float start_heading = 0.0; // TODO -- current heading
            float start_fov = M_PI/3;
            double budget;
            if(ipp_wrapper.Replan(start_point, budget)){
                ROS_ERROR_STREAM("Running GCB::"<<budget);
                ipp_wrapper.CleanUpVPList();
                ipp_wrapper.gcb_.GCB(start_point,start_heading,start_fov,0,sgmap.GetMem(),budget);
                ipp_wrapper.gcb_.path_.path_.erase(ipp_wrapper.gcb_.path_.path_.begin());
                ca_nav_msgs::PathXYZVViewPoint traj =
                        ipp_wrapper.tg_.MakeTrajectory(start_point,start_heading,ipp_wrapper.gcb_.path_,grid_frame);
                ipp_wrapper.published_traj_ = traj;

                exploration_marker_pub.publish(ipp_wrapper.GetPathMarker(1));
                traj_marker_pub.publish(ipp_wrapper.Displaytrajectory(traj));
                exploration_path_pub.publish(traj);
            }
        }
        // Visualize grid -- done        
    }
    ROS_INFO("Iter");
    visualization_msgs::Marker m =  GetSemanticGridMarker(sgmap.GetFixedGrid(), sgmap.GetMem(), sgmap.get_grid_frame());
    semantic_marker_pub.publish(m);
    m = GetSemanticGridMarkerClass(sgmap.GetFixedGrid(), sgmap.GetMem(), sgmap.get_grid_frame(), "car", ca::SemanticClass::car);
    semantic_marker_pub.publish(m);
    ros::spinOnce();
    r.sleep();
  }
  return 0;
}
